import requests
import json


def get_data(ts = None):
  url = 'https://slack.com/api/channels.history?token=xoxp-2208186609-22028983233-50745723586-ee5bdb443e&channel=C2RPF70TD&count=1000&pretty=1&latest='
  if ts:
    url += ts
  return json.loads(requests.get(url).content)

history = set([])

def links(data):
  global history
  if data['has_more']:
    ts = data['messages'][-1]['ts']
    for i in data['messages']:
      history.add(i['text'])
    return links(get_data(ts))
  else:
    return history

print len(links(get_data()))
